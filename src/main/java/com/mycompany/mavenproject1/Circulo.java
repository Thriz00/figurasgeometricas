/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import static java.lang.Math.pow;

/**
 *
 * @author sigmotoa
 */
public class Circulo extends FiguraGeometrica{
    
    private double radio;

    public Circulo(double radio) {
        this.radio = radio;
    }
    
    /**
     *
     * @return
     */
    @Override
    public double area()
    {
        return Math.PI*Math.pow(radio, 2);
        
    @Override
    
          public double perimetro()
    {
        return (Math.PI*2)*radio;
    }
    
      public double volumen()
    {
        return 4/3*(Math.PI*pow(radio,3));
    }
    
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    
}
