/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public abstract class FiguraGeometrica {
    
    public abstract double area();
    
    @Override
    public String toString ()
    {
        return "área: "+ area();
    }
     
 
      public abstract double perimetro();
    
    
    @Override
    public String toString ()
    {
        return "perimetro: "+ perimetro();
    }
     
  public abstract double volumen();
    
    /**
     *
     * @return
     */
    @Override
    public String toString ()
    {
        return "volumen: "+ volumen();
    }
     