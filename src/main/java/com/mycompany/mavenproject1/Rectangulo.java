/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Rectangulo extends FiguraGeometrica{
    private double base;
    private double altura;
    private double longitud;

    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
        this.longitud = longitud;
    }

    Rectangulo(int i, int i0, int i1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double area()
{
    return base*altura;
}
    @Override
 public double perimetro()
    {
    return (base*2)+(altura*2);
    }
 
    @Override
  public double volumen()
    {
    return base*altura*longitud;
    }
  
    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
    
    
}

